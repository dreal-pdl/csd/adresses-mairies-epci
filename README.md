# Adresses mairies epci


Ce projet a pour objectif de collecter les coordonnées des mairies et epci de l'annuaire service public sur un périmètre régional.  

![](avatar_projet.png)

Les coordonnées postales, téléphoniques et internet sont enrichies du nom des élus (maires/psdts) de ces territoires, à partir des données publiées sur data.gouv.fr (pour les maires) ou issues de la base banatic de la DGCL.